import urllib2
import urllib
import json
import logging
import uuid
import time
from datetime import datetime

class BackendException(Exception):
    pass

class BackendConnectionException(BackendException):
    pass

log = logging.getLogger('event_index_client')

class BackendSearchStatus():
    SUCCESS = 'COMPLETED'
    FAILED = 'FAILED'
    IN_PROGRESS = 'IN_PROGRESS'


class WrappedRequest(urllib2.Request):
    """A class for making backend requests. Raises appropriate errors,
    uses specified methods
    https://stackoverflow.com/questions/3884695/custom-methods-in-python-urllib2
    """
    GET = 'get'
    POST = 'post'
    PUT = 'put'
    DELETE = 'delete'
    def __init__(self, url, url_data, method=None):
        if url_data:
            url += '?' + urllib.urlencode(url_data)
        urllib2.Request.__init__(self, url)
        self.method = method

    def get_method(self):
        if self.method:
            return self.method
        return urllib2.Request.get_method(self)

    def ask(self):
        request_uuid = uuid.uuid1()
        request_begins = time.time()
        log.debug("Making request to url {url}, uuid {uuid}".format(
            url=self.get_full_url(), uuid=request_uuid))
        try:
            request_result = urllib2.urlopen(self).read()
        except urllib2.HTTPError as error:
            raise BackendException(error.read())
        except urllib2.URLError as error:
            raise BackendConnectionException(error)
        finally:
            request_ends = time.time()
            log.debug("Request to {url}, uuid {uuid} done in {time} s".format(
                url=self.get_full_url(), uuid=request_uuid, time=request_ends-request_begins))

        try:
            response = json.loads(request_result)
        except (ValueError, TypeError) as error:
            raise BackendConnectionException(
                "Can't parse the backend response")

        return response


class EventIndexClient(object):
    def __init__(self, backend_url, project):
        self.backend_url = backend_url
        self.project = project

    def start_search(self, query):
        url = "{base}/projects/{project}/_search_async".format(
            base=self.backend_url, project=self.project)
        result = WrappedRequest(url, {"query": query}, 'POST').ask()
        try:
            return result['id']
        except KeyError as error:
            raise BackendConnectionException(
                "Backend response in wrong format")


    def check_state(self, uuid):
        url = "{base}/projects/{project}/searches".format(
            project=self.project, base=self.backend_url)
        return  WrappedRequest("{url}/{uuid}/_status".format(
                url=url, uuid=uuid), None, 'GET').ask()


    def get_search_result(self, search_task_id, limit, offset):
        url = "{base}/projects/{project}/searches/{search_task_id}".format(
            base=self.backend_url, project=self.project, search_task_id=search_task_id)
        backend_response = WrappedRequest(url, {'offset': offset, 'limit': limit},
                                          'GET').ask()
        return backend_response


    def get_factor_names(self, filter_, query):
        """
        Returns a list of factor names
        @rtype: list
        """
        url = "{base}/projects/{project}/_fields".format(
            base=self.backend_url, project=self.project)
        data = {'query': query}
        if filter_ is not None:
            data['filter'] = filter_
        return WrappedRequest(url, data, 'POST').ask()


    def get_factor_values(self, filter_, factor, query):
        """
        Returns a list of factor values for a factor.
        @rtype: list
        """
        data = {'field': factor}
        if filter_ is not None:
            data['filter'] = filter_
        if query is not None:
            data['query'] = query
        url = "{base}/projects/{project}/_terms".format(
            base=self.backend_url, project=self.project)
        return WrappedRequest(url, data, method='POST').ask()


    def check_query(self, query):
        url = "{base}/projects/{project}/_check".format(
            base=self.backend_url, project=self.project)
        return WrappedRequest(url, {"query": query}, 'POST').ask()


    def get_database_statistics(self):
        url = "{base}/status".format(base=self.backend_url)
        backend_response = WrappedRequest(url, None, 'GET').ask()
        try:
            backend_response['events_count'] = backend_response['stats'][project]
        except KeyError:
            raise BackendException(json.dumps({
                'code': 404,
                'message': "Project %s not found" % project}))
        return backend_response


    def get_event_by_id(self, event_id):
        url = "{base}/projects/{project}/events/{event_id}".format(
            base=self.backend_url, project=self.project, event_id=event_id)
        return WrappedRequest(url, None, 'GET').ask()

    def histogram(self, backend_uuid, field, intervals):
        url = "{base}/projects/{project}/histo/{search_id}".format(
            base=self.backend_url, project=self.project, search_id=backend_uuid)
        histogram = WrappedRequest(
            url, {"field": field, "intervals": intervals}, 'GET').ask()
        if not "histo" in histogram:
            return histogram
        response = {"data": [], "ticks": []}
        for point in histogram['histo']:
            response['data'].append(point['count'])
            response['ticks'].append(point['value'])
        return response


    def get_histogrammable_fields(self):
        url = "{base}/projects/{project}/_histogrammable".format(
            base=self.backend_url, project=self.project)
        return WrappedRequest(url, None, 'POST').ask()['histogrammable']


    def get_search_suggestions(self, query, caret):
        url = "{base}/projects/{project}/_suggest".format(
            base=self.backend_url, project=self.project)
        return WrappedRequest(url, {'query': query, 'caret': caret}, 'POST').ask()
