from distutils.core import setup
setup(name='event_index_client',
      version='1.0',
      description='Python API client for Event Index https://eindex.cern.ch/ ',
      author='Nikita Kazeev',
      author_email='kazeevn@yandex-team.ru',
      url='https://gitlab.cern.ch/YDF/event_index_client',
      packages=['event_index_client']
      )
